import socket
import select

port = 60003
sockL = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sockL.bind(("", port))
sockL.listen(2)

listOfSockets = [sockL]
print("lyssnar på port {}".format(port))

def sendToAll(data, message):
    #skicka meddelande till alla klienter
    for i in listOfSockets:
        if i == sockL:
            continue
        else:
            i.sendall(bytearray("{} ".format(data) + message, "ascii"))


while True:
    tup = select.select(listOfSockets, [], [])
    sock = tup[0][0]

    if sock == sockL:
        #ny client ansluter
        (sockClient, addr) = sockL.accept()
        listOfSockets.append(sockClient)
        data = sockClient.getpeername()
        sendToAll(data, " uppkopplad\n")
    else:
        try:
            #existerande klient skickar data eller kopplar från
            data = sock.recv(2048)
            if not data:
                sendToAll(sockClient.getpeername(), " nedkopplad\n")
                sock.close()
                listOfSockets.remove(sock)

            else:
                sendToAll(sockClient.getpeername(), data.decode("ascii"))
        except:
            sendToAll(sockClient.getpeername(), " majskolv\n")
            sock.close()
            listOfSockets.remove(sock)


